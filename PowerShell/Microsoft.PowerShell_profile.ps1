function prompt
{
    #Write-Host ("PS " + $(Get-Date -uFormat "%H%M") + " " + $($PWD.ProviderPath) + ">") -nonewline
    #return " "
    Write-Host ("[pwsh]" + " " + $($PWD.ProviderPath) + " >>") -nonewline 
    return " "
}

$zipdir = Get-Item "D:\Smieci\zip\";
$xdir = Join-Path $home "\Pictures\Jadisco_xd\";

function mpvyt {mpv --ytdl-format=248+251/247+251/bestvideo[ext=webm]+bestaudio[ext=webm]/best --no-pause  $args }
function mpvytlow {mpv --ytdl-format=397+250/244+250/396+250/243+250/135+140/134+140/43/best --no-pause  $args }
function ydl {youtube-dl --merge-output-format mkv -f 303+251/302+251/248+251/247+251/244+251/best --external-downloader aria2c --external-downloader-args "-R -x16 -j16 -s16 -k 1M" --output "%(upload_date)s %(title)s-%(id)s.%(ext)s"  $args}
function ydlw {youtube-dl --merge-output-format mkv -f "bestvideo[ext=webm][height<=720]+bestaudio[ext=webm]/bestvideo[ext=webm][height<=720]+bestaudio" --external-downloader aria2c --external-downloader-args "-R -x16 -j16 -s16 -k 1M" --output "%(upload_date)s %(title)s-%(id)s.%(ext)s"  $args}
function ydlsub($lang = "en") {youtube-dl --merge-output-format mkv -f 303+251/302+251/248+251/247+251/244+251/best --external-downloader aria2c --external-downloader-args "-R -x16 -j16 -s16 -k 1M" --output "%(upload_date)s %(title)s-%(id)s.%(ext)s" --embed-sub --convert-subs srt --write-sub --sub-lang $lang $args}
function ydlop {youtube-dl -f 251/250 -x --audio-format opus --external-downloader aria2c --external-downloader-args "-R -x8 -j8 -s8 -k 1M" --output "%(upload_date)s %(title)s-%(id)s.%(ext)s"  $args}
function ffop($link, $output) {ffmpeg -i "$([string]$link)" -c:a libopus -b:a 40k -vbr on -af aresample=resampler=soxr -ar 48000 -ac 1 -map 0:a:0 $args "$([string]$output + ".opus")"}
function ff4a($link, $output) {ffmpeg -i "$([string]$link)" -c:a aac -b:a 64k -ac 1 -map 0:a:0 $args "$([string]$output + ".mp4")"}
function ffogg($link, $output) {ffmpeg -i "$([string]$link)" -c:a libvorbis -q:a 3 -ac 1 -map 0:a:0 $args "$([string]$output + ".ogg")"}
function 7ztar($files = "*") {7z a -t7z -mx0 "$("..\" + [string]$(split-path $pwd -Leaf) + ".7z")" $files $args}
function kurwa {Write-Host "(Get-Command Show-List).definition"}
function Base64Decode($base64string) {$decoded = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($base64string)); Write-Host $decoded; $decoded | clip}

function Base64Glue
{
    ForEach ($item in $args)
    {
        [string]$decoded = $decoded + [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($item))
    }
    Write-Host $decoded;
    $decoded | clip;
}

function mpvrec
{
    [CmdletBinding(PositionalBinding=$false)]
    param(
        [Parameter(Mandatory = $true, Position = 0)] [string] $Link,
        [string] $Name = "$("Recording." + $(youtube-dl.exe  $Link --output "%(uploader)s" --get-filename) + "." + $(Get-Date -uFormat "%Y%m%d.%H%M%S") + ".ts")",
        [parameter(Mandatory = $false, ValueFromRemainingArguments = $true)] [string] $Rest
    )

    mpv --no-pause --record-file="$Name" $Link $Rest
}

function webmPodcast($link, $extension="webm")
{
    $stringName = [string] $($(Get-ItemProperty $link).BaseName);
    $caption = $stringName.Split(' ',2)[1];
    magick convert -size 900x200 -gravity Center -pointsize 40 -font Rubik-Medium -background transparent -fill white caption:"$caption" tempBackgroundText.png;
    magick composite -gravity South tempBackgroundText.png "C:\Users\bltzkrg22\Documents\WindowsPowerShell\Background.png" tempBackgroundCombined.png;
    if ($extension -eq "webm")
    {
        ffmpeg -r 1 -y -loop 1 -i tempBackgroundCombined.png -i $link -c:v libvpx-vp9 -pix_fmt yuva420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + $extension)";
    }
    elseif ($extension -eq "ogg")
    {
        ffmpeg -r 1 -y -loop 1 -i tempBackgroundCombined.png -i $link -c:v libtheora -qscale:v 9 -pix_fmt yuva420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + $extension)";
    }
    elseif ($extension -eq "mp4")
    {
        ffmpeg -r 1 -y -loop 1 -i tempBackgroundCombined.png -i $link -c:v libx264 -pix_fmt yuv420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + $extension)";
    }
    elseif ($extension -eq "av1")
    {
        ffmpeg -r 1 -y -loop 1 -i tempBackgroundCombined.png -i $link -c:v libaom-av1 -strict experimental -cpu-used 3 -crf 22 -pix_fmt yuv420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + "mp4")";
    }
    Remove-Item tempBackgroundText.png;
    Remove-Item tempBackgroundCombined.png;
}
function webmSong($link, $extension="webm")
{
    $stringName = [string] $($(Get-ItemProperty $link).BaseName);
    $caption = $stringName.Split(' ',2)[1];
    magick convert -size 900x200 -gravity Center -pointsize 40 -font Rubik-Medium -background transparent -fill white caption:"$caption" tempBackgroundText.png;
    magick composite -gravity South tempBackgroundText.png "C:\Users\bltzkrg22\Documents\WindowsPowerShell\BackgroundM.png" tempBackgroundCombined.png;
    if ($extension -eq "webm")
    {
        ffmpeg -r 1 -y -loop 1 -i tempBackgroundCombined.png -i $link -c:v libvpx-vp9 -pix_fmt yuva420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + $extension)";
    }
    elseif ($extension -eq "ogg")
    {
        ffmpeg -r 1 -y -loop 1 -i tempBackgroundCombined.png -i $link -c:v libtheora -qscale:v 9 -pix_fmt yuva420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + $extension)";
    }
    elseif ($extension -eq "mp4")
    {
        ffmpeg -r 1 -y -loop 1 -i tempBackgroundCombined.png -i $link -c:v libx264 -pix_fmt yuv420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + $extension)";
    }
    elseif ($extension -eq "av1")
    {
        ffmpeg -r 1 -y -loop 1 -i tempBackgroundCombined.png -i $link -c:v libaom-av1 -strict experimental -cpu-used 3 -crf 22 -pix_fmt yuv420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + "mp4")";
    }
    Remove-Item tempBackgroundText.png;
    Remove-Item tempBackgroundCombined.png;
}
function webmCover($link, $coverlink, $extension="webm")
{
    $stringName = [string] $($(Get-ItemProperty $link).BaseName);
    magick convert -resize 600x300 $coverlink  tempCover.png;
    magick convert -size 600x300 xc:"rgba(0,0,0,0)" PNG32:tempTrans.png
    magick composite -gravity center tempCover.png tempTrans.png tempCoverCombined.png;
    if ($extension -eq "webm")
    {
        ffmpeg -r 1 -y -loop 1 -i tempCoverCombined.png -i $link -c:v libvpx-vp9 -pix_fmt yuva420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + $extension)";
    }
    elseif ($extension -eq "ogg")
    {
        ffmpeg -r 1 -y -loop 1 -i tempCoverCombined.png -i $link -c:v libtheora -qscale:v 9 -pix_fmt yuva420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + $extension)";
    }
    elseif ($extension -eq "mp4")
    {
        ffmpeg -r 1 -y -loop 1 -i tempCoverCombined.png -i $link -c:v libx264 -pix_fmt yuv420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + $extension)";
    }
    elseif ($extension -eq "av1")
    {
        ffmpeg -r 1 -y -loop 1 -i tempCoverCombined.png -i $link -c:v libaom-av1 -strict experimental -cpu-used 3 -crf 22 -pix_fmt yuv420p -c:a copy -shortest -map_metadata 1:s:a "$($stringName + "." + "mp4")";
    }
    Remove-Item tempCover.png;
    Remove-Item tempTrans.png
    Remove-Item tempCoverCombined.png;
}

function MakeAPDFA
{
    Get-ChildItem *jpg | ForEach-Object {magick mogrify -format pdfa "$_" }
    Get-ChildItem *tif | ForEach-Object {magick mogrify -format pdfa "$_" }
    Get-ChildItem *png | ForEach-Object {magick mogrify -format pdfa "$_" }
    $list = $(Get-ChildItem *pdfa  | ForEach-Object {$_.Name})
    [string]$newpdfname = $(Get-Date -uFormat "%Y%m%d.%H%M%S") + ".pdf"
    Copy-Item "C:\Ghostscript\resources\PDFA_def_my.ps" .\PDFA_def_my.ps
    Copy-Item "C:\Ghostscript\resources\AdobeRGB1998.icc" .\AdobeRGB1998.icc
    Set-ItemProperty .\PDFA_def_my.ps -name IsReadOnly -value $false
    Set-ItemProperty .\AdobeRGB1998.icc -name IsReadOnly -value $false
    gswin64c.exe -P -dPDFA -dBATCH -dNOPAUSE -sProcessColorModel=DeviceRGB -sDEVICE=pdfwrite -sPDFACompatibilityPolicy=1 "-sOutputFile=$newpdfname" .\PDFA_def_my.ps $list
    Remove-Item .\PDFA_def_my.ps
    Remove-Item .\AdobeRGB1998.icc
}

function MakeAPDF
{
    Get-ChildItem *jpg | ForEach-Object {magick mogrify -format pdf "$_" }
    Get-ChildItem *tif | ForEach-Object {magick mogrify -format pdf "$_" }
    Get-ChildItem *png | ForEach-Object {magick mogrify -format pdf "$_" }
    $list = $(Get-ChildItem *pdf  | ForEach-Object {$_.Name})
    [string]$newpdfname = $(Get-Date -uFormat "%Y%m%d.%H%M%S") + ".pdf"

    if (Test-Path .\pdfmarks)
    {
        gswin64c.exe -P -dBATCH -dNOPAUSE -sDEVICE=pdfwrite "-sOutputFile=$newpdfname" $list pdfmarks
    }
    else
    {
        gswin64c.exe -P -dBATCH -dNOPAUSE -sDEVICE=pdfwrite "-sOutputFile=$newpdfname" $list
    }
}

function dotnetcon {dotnet new console --language "C#"}

$magia = Join-Path (Get-ItemProperty $profile).directory "\tools-imagemagick.psm1";
Import-Module $magia;
Import-Module (Join-Path (Get-ItemProperty $profile).directory "\odbanowanko.psm1");
$historyfile = $env:APPDATA + "\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt";