function odbanuj($Time=3600,$Date)
{
    # dzisiejsza data w formacie takim, jak w logach (trzyliterowy skrót miesiąca po angielsku + numer dnia z wiądącym zerem)
    # nie chce mi się obsługiwać ostatniego dnia miesiąca, sięc $DateNext będzie wtedy niepoprawny
    if ( [string]::IsNullOrEmpty($Date) ) {$Date = (Get-Date).ToString("MMM dd",[CultureInfo]'us')};
    [string]$DateNext = $Date.split()[0] + " " + $([int]$Date.split()[1] + 1);

    # plik z logiem
    $logfile = "c:\Users\bltzkrg22\AppData\Roaming\HexChat\logs\irc.poorchat.net\#jadisco.log"

    New-Variable tempbanlist
    New-Variable banlist
    New-Variable unbanlist

    # out-null na końcu, bo wcześniej chciałem zrobić jeszcze jakąś operację, ale już tak zostało
    Get-Content $logfile | Select-String " banned"," muted"," unbanned" | Tee-Object -Variable tempbanlist | Select-String "no longer banned"," unbanned" | Select-String $DateNext | Tee-Object -Variable unbanlist | Out-Null
    $tempbanlist | Select-String $Date | Tee-Object -Variable banlist | Out-Null

    # HashSet: https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.hashset-1?view=netframework-4.8
    $bannset = New-Object System.Collections.Generic.HashSet[string]

    foreach ($line in $banlist) {
        $linetemp = ([string]$line).Substring(18);

        # jeżeli Wonziu zbanował, dodaj do zbioru zbanowanych
        $banner = $linetemp.split()[0];
        if ($banner -eq "Wonziu") {
            $action = $linetemp.split()[1];
            if ($action -eq "banned")
            {
                $user = ($linetemp.split()[2]).trimEnd(".");
                $bannset.Add($user) | Out-Null;
            }
        }
        Remove-Variable banner -ErrorAction:SilentlyContinue
        Remove-Variable action -ErrorAction:SilentlyContinue
        Remove-Variable user -ErrorAction:SilentlyContinue
        # jeżeli ktoś odbanował, usuń ze zbioru zbanowanych
        if ($linetemp -match "^\w+ unbanned \w+.")
        {
            $user = ($linetemp.split()[2]).trimEnd(".");
            $bannset.Remove($user) | Out-Null;
        }
        if ($linetemp -match "is no longer banned.")
        {
            $user = ($linetemp.split()[0]).trimEnd(".");
            $bannset.Remove($user) | Out-Null;
        }
    }
    foreach ($line in $unbanlist) {
        $linetemp = ([string]$line).Substring(18);

        # jeżeli ktoś odbanował następnego dnia, usuń ze zbioru zbanowanych
        if ($linetemp -match "^\w+ unbanned \w+.")
        {
            $user = ($linetemp.split()[2]).trimEnd(".");
            $bannset.Remove($user) | Out-Null;
        }
        if ($linetemp -match "is no longer banned.")
        {
            $user = ($linetemp.split()[0]).trimEnd(".");
            $bannset.Remove($user) | Out-Null;
        }
    }

    # każdego zbanowanego zmutować na $Time sekund
    foreach ($user in $bannset) {
        Write-Host ".unban $user";
        Write-Host ".mute $user $Time"
    }
}