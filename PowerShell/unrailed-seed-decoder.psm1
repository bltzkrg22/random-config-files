function Decode-Seed ()
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateLength(7, 7)]
        [string]$Seed
    )

    $difficultyChar = $Seed.Substring(5,1)
    $modeChar = $Seed.Substring(6,1)

    $difficultyLookup = New-Object System.Collections.Generic.Dictionary"[Char,String]"
    $difficultyLookup['4'] = "Kids";
    $difficultyLookup['o'] = "Kids";
    $difficultyLookup['I'] = "Kids";
    $difficultyLookup['Y'] = "Kids";

    $difficultyLookup['g'] = "Easy";
    $difficultyLookup['w'] = "Easy";
    $difficultyLookup['A'] = "Easy";
    $difficultyLookup['Q'] = "Easy";

    $difficultyLookup['i'] = "Medium";
    $difficultyLookup['y'] = "Medium";
    $difficultyLookup['C'] = "Medium";
    $difficultyLookup['S'] = "Medium";

    $difficultyLookup['k'] = "Hard";
    $difficultyLookup['0'] = "Hard";
    $difficultyLookup['E'] = "Hard";
    $difficultyLookup['U'] = "Hard";

    $difficultyLookup['m'] = "Extreme";
    $difficultyLookup['2'] = "Extreme";
    $difficultyLookup['G'] = "Extreme";
    $difficultyLookup['W'] = "Extreme";

    $modeLookup = New-Object System.Collections.Generic.Dictionary"[Char,String]"
    $modeLookup['s'] = "Endless";
    $modeLookup['Y'] = "Quick";
    $modeLookup['g'] = "Sandbox";
    $modeLookup['U'] = "Timed";
    $modeLookup['I'] = "Versus";

    if ($difficultyLookup.ContainsKey($difficultyChar) -eq $false) 
    {
        Write-Error "Difficulty (6th) character not recognized. Check the seed.";
    }
    elseif ($modeLookup.ContainsKey($modeChar) -eq $false) 
    {
        Write-Error "Game mode (7th) character not recognized. Check the seed.";
    }
    else
    {
        Write-Host "$($Seed + " is: " + $difficultyLookup[$difficultyChar] + " " + $modeLookup[$modeChar])";
    }
}
