function webpify80($inputfile, $outputfile) {
    # if $outputfile is empty, use ($inputfile + webp extension); the exstensions will stack in that case, so no overwriting is expected
    if ( [string]::IsNullOrEmpty($outputfile) ) {$outputfile = $inputfile};
    magick convert "$inputfile" -quality 80 -define webp:auto-filter=true -define webp:method=6 "$([string]$outputfile + ".webp")"
}

function webpify80s($inputfile, $outputfile) {
    # if $outputfile is empty, use ($inputfile + webp extension); the exstensions will stack in that case, so no overwriting is expected
    if ( [string]::IsNullOrEmpty($outputfile) ) {$outputfile = $inputfile};
    magick convert "$inputfile" -quality 80 -define webp:auto-filter=true -define webp:method=6 -define webp:use-sharp-yuv=1 "$([string]$outputfile + ".webp")"
}

function webpifylosless($inputfile, $outputfile) {
    # if $outputfile is empty, use ($inputfile + webp extension); the exstensions will stack in that case, so no overwriting is expected
    if ( [string]::IsNullOrEmpty($outputfile) ) {$outputfile = $inputfile};
    magick convert "$inputfile" -define webp:lossless=true  -define webp:auto-filter=true  -define webp:method=6 "$([string]$outputfile + ".webp")"
}

function flifyfy($inputfile, $outputfile) {
    # if $outputfile is empty, use ($inputfile + webp extension); the exstensions will stack in that case, so no overwriting is expected
    if ( [string]::IsNullOrEmpty($outputfile) ) {$outputfile = $inputfile};
    magick convert "$inputfile" "$([string]$outputfile + ".flif")"
}

function webpifyanim80($inputfile, $outputfile) {
    if ( [string]::IsNullOrEmpty($outputfile) ) {$outputfile = $inputfile};
    ffmpeg -i "$inputfile" -loop 0 -lossless 0 -q:v 80 -compression_level 6 -pix_fmt yuva420p "$([string]$outputfile + ".webp")"
}

function webpifyaniml($inputfile, $outputfile) {
    if ( [string]::IsNullOrEmpty($outputfile) ) {$outputfile = $inputfile};
    ffmpeg -i "$inputfile" -loop 0 -lossless 1 -compression_level 6 -pix_fmt yuva420p "$([string]$outputfile + ".webp")"
}

function roundcorners($inputfile, $outputfile, $radius=22) {
    if ( [string]::IsNullOrEmpty($outputfile) ) {$outputfile = [System.IO.Path]::GetFileNameWithoutExtension($inputfile) + ".rounded.png"};

    $width = magick identify -format "%w" $inputfile;
    $height = magick identify -format "%h" $inputfile;
    $maskfile = [System.IO.Path]::GetTempFileName() + ".png";

    magick convert -size "$($width + "x" + $height)" xc:transparent -draw "$("roundrectangle 0,0," + $width + "," + $height + "," + $radius + "," + $radius)" $maskfile;

    magick convert $inputfile -matte $maskfile -compose DstIn -composite $outputfile;

    Remove-Item $maskfile;
}